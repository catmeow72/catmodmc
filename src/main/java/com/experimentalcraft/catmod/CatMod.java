package com.experimentalcraft.catmod;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.item.v1.FabricItemSettings;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.experimentalcraft.catmod.items.CatFood;

public class CatMod implements ModInitializer {
	public static final Logger LOGGER = LoggerFactory.getLogger("catmod");

	public static final CatFood CAT_FOOD_ITEM = new CatFood(new FabricItemSettings().group(ItemGroup.FOOD));

	@Override
	public void onInitialize() {

		LOGGER.info("Catmeow's mod is initiializing...");
		Registry.register(Registry.ITEM, new Identifier("catmod", "cat_food"), CAT_FOOD_ITEM);
		LOGGER.info("Done!");
	}
}
